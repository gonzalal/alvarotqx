# Run arguments file auto-generated on Wed Feb 16 18:16:12 2022 by:
# JobTransform: EVNTtoDAOD
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'EVNTtoDAOD' 

runArgs.reductionConf = ['TRUTH1']

# Explicitly added to process all events in this step
runArgs.maxEvents = -1

# Input data
runArgs.inputEVNTFile = ['evgen.root']
runArgs.inputEVNTFileType = 'EVNT'
runArgs.inputEVNTFileNentries = 1000L
runArgs.EVNTFileIO = 'input'

# Output data
runArgs.outputDAOD_TRUTH1File = 'DAOD_TRUTH1.test.pool.root'
runArgs.outputDAOD_TRUTH1FileType = 'AOD'

# Extra runargs

# Extra runtime runargs

# Literal runargs snippets
